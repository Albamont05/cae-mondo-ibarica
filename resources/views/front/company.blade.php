<x-front-layout section="empresa">
	<h2 class="shadow-xl border border-gray-100 rounded-lg px-6 py-4 text-blue uppercase font-semibold font-bebas text-2xl tracking-wider mb-4">Rechazados</h2>
	<table class="table-auto w-full">
		<thead class="text-left text-black border-b-2 border-black text-lg">
			<tr>
				<th>Documento</th>
				<th>Estado</th>
				<th>Fecha de subida</th>
				<th></th>
			</tr>
		</thead>
		<tbody class="font-semibold leading-8">
			<tr>
				<td>CIF / NIF / NIE</td>
				<td><x-badge color="red">Rechazado</x-badge></td>
				<td>04/03/2022</td>
			</tr>
			<tr>
				<td>Escritura de constitución de empresa y apoderamiento</td>
				<td>Rechazado</td>
				<td>04/03/2022</td>
			</tr>
			<tr>
				<td>Alta Seguridad Social</td>
				<td>Rechazado</td>
				<td>04/03/2022</td>
			</tr>
		</tbody>
	</table>


</x-front-layout>